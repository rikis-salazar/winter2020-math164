# Practice material

Unfortunately, I do not have _old exams_ that I can release at this point.
Instead, here you will find a collection of exercises that should represent the
degree of difficulty, as well as the course topics that you can expect in our
midterms.

Please note that, as the name of this section implies, exercises posted here are
to be regarded as practice material. There is no guarantee the midterm problems
will look like the ones that will be eventually listed below.

---

<!-- This is a comment

## Practice for final exam

_Status:_ Final.

> Additional exercises will not be added to this list.

1.  Assume $u$ is _harmonic_ in a domain $G$, and assume that for a given $w\in
    G$ the set $\{z\in\mathbb{C}\,:\, |z-w|\le r \}$ is completely contained in
    $G$ for some $r>0$.

    Prove that
    $$u(w)=\frac{1}{2\pi}\int_{0}^{2\pi} u(w+r\mathrm{e}^{it})\,\mathrm{d}t.$$

    In other words, the value of a harmonic function at the center of a disk is
    the average over the values at the boundary of the disk.

    > _Hint:_ A similar result holds for analytic functions. Construct $f$
    > analytic such that $u = \mathbf{Re}(f)$.

    > **Solution:**
    >
    > Recall that given a harmonic function $u$, it is possible to construct a
    > function $v$ such that $f = u + iv$ is analytic. Let $f$ be such a
    > function, then for $w\in G$
    > $$f(w)=\frac{1}{2\pi}\int_{0}^{2\pi} f(w+r\mathrm{e}^{it})\,\mathrm{d}t.$$
    > The result then follows by taking the real part of the equation above.

---

## Practice for midterm 2

_Status:_ Final.

> Additional exercises will not be added to this list.

1.  Find the lengths of the following paths:

    i.  $\gamma(t) = i + \mathrm{e}^{i\pi t}$, $0 \le t \le 1$.
    i.  $\gamma(t) = i\sin(t)$, $-\pi \le t \le \pi$.

    > **Solution:**
    >
    > i.  The length is
    >     $$\mathcal{L} = \int_{0}^{1} |\gamma'(t)|\,\mathrm{d}t =
    >       \int_{0}^{1} | i\pi\mathrm{e}^{i\pi t}|\,\mathrm{d}t =
    >       \int_{0}^{1} \pi\,\mathrm{d}t = \pi.$$

---

## Practice for midterm 1

_Status:_ Final.  

> Additional exercises will not be added to this list.

1.  Find all the solutions of the equation $z^{5} - 1 = 0$; then explain why it
    is true that

    $$z^{5} - 1 = (z-1)(z^{2}+2z\cos(\pi/5)+1)(z^{2}-2z\cos(2\pi/5)+1).$$

    > **Solution:**
    >
    > The roots of the equation are $z_{1}=1$, $z_{2} = \mathrm{e}^{2\pi/5}$,
    > $z_{3} = \mathrm{e}^{4\pi/5}$, $z_{4} = \bar{z_{3}}$, and $z_{5} =
    > \bar{z_{2}}$. Why?
    >

---

This is the end of the comment. -->



[Return to main course website][MAIN]


[book2]: http://math.sfsu.edu/beck/complex.html
[MAIN]: ..

