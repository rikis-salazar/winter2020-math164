# Miscellaneous handouts

In this section you will find material that is related to the contents we are
discussing during lecture. In some cases the material presented here can also be
found in your textbook (albeit with a slightly different notation); whereas in
other cases, the material has been adapted from other sources. It is my hope
that at some point you take the time to read these handouts, and that they
provide a different perspective that you can take advantage of.

1.  [_Didn't we take care of "optimization" back in Calculus anyway?_][h1]
1.  [_Golden section search & Fibonacci method_][h2]

---

[Return to main course website][MAIN]


[h2]: handout2/
[h1]: handout1/

[MAIN]: ..

