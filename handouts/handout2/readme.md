# _One dimensional search methods: golden section & Fibonacci_

> **Note:**
>
> This handout is mostly base on the material found in _Chong_ and _Zak_
> (Chapter 7). Although the presentation order and notation differs slightly,
> from the one on the textbook, the ideas are essentially the same. The emphasis
> in this document is placed on how different selection of _interior_ points
> leads to different methods, each of which presents a different set of
> advantages and disadvantages.

_Basic idea:_ given a _unimodal_ function and an initial _box_, we want to
construct _smaller_ boxes, making sure the optimal value (_local minimizer_ in
this case) remains _trapped_ within these boxes.

## The outline

*   Point out that given a box $[l_{0},r_{0}]$, a minimum of two _interior
    points_ (say $a_{1}$ and $b_{1}$) are needed to _pinpoint_ the local
    minimizer and _trap it_ within a smaller box. In this case the interior
    points will be chosen symetrically with respect to the center of the
    original box.

    -   Explain that if only one interior point is used, it is impossible to
        determine whether the minimizer is on the resulting left, or right
        sub-box.
    
*   Carefully explain that the _candidate_ boxes to trap the minimizer are
    $[l_{1},r_{1}]=[l_{0},b_{1}]$, or $[l_{1},r_{1}]=[a_{1},b_{0}]$. In either
    case, the new _box_ $[l_{1},r_{1}]$ has been reduced by a $\rho_{1}$ factor.
    In other words, if the original box has length 1, any of the smaller boxes
    has length $1-\rho_{1}$.

    -   Indicate that the process just described can then be repeated on the
        smaller box $[l_{1},r_{1}]$ to produce a smaller box $[l_{2},r_{2}]$.
        _Rinse, repeat,..._

        +    Problem: evaluating the objective function might be costly and we
             are wasting evaluations at many interior points.

        +    Solution: reuse one of the interior points. If the left _sub-box_
             is selected to become $[l_{1},r_{1}]$, then let the other interior
             point _carry over_ to the next step in the process. In other words,
             if out of $a_{1}$ and $b_{1}$, the right interior point $b_{1}$
             becomes the right end of the smaller _sub-box_ (_i.e.,_
             $[l_{1},r_{1}]=[l_{0},b_{1}]$), then let the next right interior
             point $b_{2}$ be set at $a_{1}$; this way only one more interior
             point, in this case $a_{2}$, needs to be evaluated.

             On the other hand, if the right _sub-box_ is selected to become
             $[l_{1},r_{1}]$ (_i.e.,_ $[l_{1},r_{1}]=[a_{1},r_{0}]$) then let
             $a_{2} = b_{1}$, then proceed to find $b_{2}$.
             
             As in the previous case, the smaller nested _sub-box_
             $[l_{2},r_{2}]$ has length $(1-\rho_{2})\times\text{lenght of
             }[l_{1},r_{1}]= (1-\rho_{2})(1-\rho_{1})$.

    -   Rinse, repeat, ...

*   After taking care of the inefficiencies in the selection of the interior
    points, the next question should be how to select $\rho_{k}$ at the $k$-th
    step in the process:

    -   One possible way is to let all of the $\rho_{k}$ values be equal. This
        leads to $\rho \approx 0.38$ (more details will be provided here).

    -   A second possible way calls for the minimization of the length of the
        $k$-th _sub-box_. That is, we want the quantity
        $(1-\rho_{1})(1-\rho_{2})\cdots(1-\rho_{k})$ subject to a linear set of
        constraints to be minimized: this leads to the _Fibonacci_ method (it is
        OK if the non-linear problem is not solved at this point).
        
---

[Click here to access a `.pdf` version of this handout][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

