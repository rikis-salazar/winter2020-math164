# _Didn't we take care of "optimization" back in Calculus anyway?_

> The short answer: No.  
> The long answer: _It's complicated._  

First of all, let us settle on a rough idea of the meaning of _optimization_.
According to your textbook:

>  [An optimization problem] can be viewed as a decision problem  that involves
>  finding [the "best" of] the decision variables over all [possibilities.]

In practice this amounts to solving statements of the form:

*   _Find the maximum/minimum of $f(x)$ over the set..._

At this point I am certain most individuals reading these lines are thinking
something along the lines of

> "This course should be a piece of cake. We can always resort to the 1st
> derivative test to obtain possible candidates for local extrema; then we can
> rely on the 2nd derivative test to determine whether these candidates are
> local maxima/minima."

While it is certainly true that a rather big set of optimization problems can be
_tackled_ in a way that more of less follows the _thought process_ described
above (1st + 2nd derivative tests[^one]), one should keep in mind that _the
devil is in the details_, as there are optimization problems where these tests
are either useless, or cannot be applied to the original (unmodified) problem.

To illustrate some of these issues, consider the following problem:
\begin{align*}
  & \text{maximize} & f(x) & = mx + b, \\
  & \text{subject to} & & x\in\mathbb{R}.
\end{align*}
Clearly, this problem has no solution if $m\neq 0$. On the one hand, if $m>0$,
regardless of our choice of $x$, there will always be at least one other point
$\tilde{x}>x$ such that $f(\tilde{x}) > f(x)$. On the other hand, a similar
argument shows that if $m<0$, _smaller_ points will result in _bigger_ values;
moreover the _biggest_ value cannot be realized via a _real_ point.

Unsurprisingly, the 1st + 2nd derivative tests from calculus are not useful at
all.

On a similar note, consider the following problem:
\begin{align*}
  & \text{maximize} & f(x) & = mx + b, \\
  & \text{subject to} & 1-x & \ge 0, \\
  &                   & x &\ge 0,
\end{align*}
where the set of linear inequalities $1-x \ge 0$, $x \ge 0$, are just ~~a
fancy~~ an equivalent way to express the _constraint_ $x\in[0,1]$.

Just like in the _unconstrained_ problem above, the 1st + 2nd derivative tests
are useless (Why?). However, unlike in the previous case, this problem admits a
_unique_ maximizer if $m\neq 0$. Namely, $x = 0$ for positive values of $m$, and
$x=1$ for negative values of $m$.

The two optimization problems described above illustrate some of the points that
we will encounter in the next few weeks:

*   The 1st + 2nd derivative tests will come in handy when the function we want
    to optimize (_a.k.a._ objective function) is not linear and the problem is
    _unconstrained_. For the case of linear objectives, we can expect linear
    algebra techniques to be more useful than calculus techniques.
*   In the _unconstrained_ case, if the objective looks locally like a quadratic
    function, then we should expect to find one local optimizer (minimum, or
    maximum).
*   In the case of a _linearly constrained_ problem (_i.e.,_ the set of
    restrictions on the variables is given by a set of linear equations), via
    reduction in the number of _non-free_ variables, the original problem might
    redice to an unconstrained one, where the usual 1st + 2nd derivatives tests
    could be applied.
*   In the _constrained_ case (linear or not), the _boundary_ of the set of
    constraints might be worth inspecting for possible optimizers (_e.g.,_
    $x=0$, and $x=1$ in the constrained problem discussed above.

 And finally,
    
*   In the _constrained_ case (linear or not), we might need to resort to
    studying all possible subsets of _active_ constraints. In other words, it
    might worth cheking out several different similar optimization problems were
    some of the original constraints are _intentionally ignored_.
   


[^one]: These tests are also known as **FONC/SONC** (**F**irst/**S**econd **O**rder
  **N**ecessary **C**ondition), and **SOSC** (**S**econd **O**rder **S**ufficient
  **C**ondition) 

---

[Click here to access a `.pdf` version of this handout][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

