# MATH 164: Optimization (Winter 2020)

This is to be considered the class website. Here you will find material related
to the concepts we discuss during lecture.

## Sections

*   [The class syllabus][syllabus] ([`.pdf` version][syllabus-pdf])
*   [Weekly planning][plans]
*   [Practice material: old exams and/or exercises][practice]
*   [Homework assignments][hw]
*   [Course handouts][handouts]
*   [Quiz solutions][qs]


[syllabus]: syllabus/
[syllabus-pdf]: syllabus/readme.pdf
[hw]: assignments/
[plans]: planning/
[practice]: practice/
[handouts]: handouts/
[qs]: https://www.kualo.co.uk/404


## Additional resources

Here are other sites associated to our course:

*   [The CCLE class website][CCLE]: this will be used mostly for class
    announcements.

*   [Discussion section material][ds]: Your TA might decide to post discussion
    material for the benefit of the class as a whole.


[CCLE]: https://ccle.ucla.edu/course/view/20W-MATH164-1
[ds]: https://www.kualo.co.uk/404
