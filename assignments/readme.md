# Homework assignments

Homework assignment problems fall into two categories: _regular_, and
_miscellaneous_ exercises.

*   _Regular_ exercises are the ones you are supposed to hand in during lecture.
    Your TA will select and grade only three of these problems.
*   _Miscellaneous_ exercises are for you to practice key concepts/tools we
    develop during lecture, and/or to help you further explore
    ideas/models/solutions that were briefly discussed during lecture.

    > These problems will not be collected (nor graded), but you are more than
    > welcome to ask me or your TA about them during office hours.

In addition, every homework assignment will be marked with either an _in
progress_, or _final_ status.

*   _In progress (status)_ means that more problems could be added to
    corresponding assignment. Once the list is complete, the old status will be
    replaced with to _final_ status (see below).
*   _Final (status)_ means that no more problems will be added to the
    corresponding assignment.

---

## Homework 6

_Status:_ In progress.  

**Due date:** Wednesday, March 13 (during lecture).

From Chong & $\dot{\mathrm{Z}}$ak, _An Introduction to Optimization_.

-   _Regular_ exercises:

    *   _Chapter 20:_ 1, 2 b c, 5, 6 a c d, 7 a, 8, 10, 15.
    *   _Chapter 21:_ 1, 2 a b, 5, 10.

-   _Miscellaneous_ exercises:
    *   _Chapter 21:_ 12, 14.

---

## Homework 5

_Status:_ ~~In progress~~ Final.  

**Due date:** Wednesday, March 4 (during lecture).

From Chong & $\dot{\mathrm{Z}}$ak, _An Introduction to Optimization_.

-   _Regular_ exercises:

    *   _Chapter 20:_ 2 a, 3, 4.

-   _Miscellaneous_ exercises:
    *   _Chapter 20:_ 17, 18.


    In addition please solve the following exercises.

    1.  Consider the problem
    \begin{align*}
       \text{minimize}\quad
         & x_{1}^{2} + x_{1}^{2}x_{3}^{2} + 2x_{1}x_{2} + x_{2}^{4} + 8x_{2} \\
       \text{subject to}\quad
         & 2x_{1} + 5x_{2} + x_{3} = 3.
    \end{align*}

        *   Determine which of the following points are _candidates_ for minimizers
    (_i.e.,_ find the ones that make the reduced gradient vanish):

            i. $\begin{bmatrix}0& 0& 2\end{bmatrix}^{T}$
            i. $\begin{bmatrix}0& 0& 3\end{bmatrix}^{T}$
            i. $\begin{bmatrix}1& 0& 1\end{bmatrix}^{T}$
    
        *   Determine whether each of the _candidates_ you found in the previous
        part are local minimizers. What can you say about those that are not?

    1.  ~~Consider~~ Solve the problem
    \begin{align*}
       \text{maximize}\quad & x_{1}x_{2}x_{3} \\
       \text{subject to}\quad
         & \frac{x_{1}}{a_{1}} + \frac{x_{2}}{a_{2}} + \frac{x_{3}}{a_{3}} = 1
           \quad(a_{1},a_{2},a_{3} > 0).
    \end{align*}

    1.  Let $\mathbb{A}$ be a matrix of _full row rank_ (_i.e.,_ its rows are
    linearly independent). Find the point in the set $\mathbb{A}x = b$ which
    minimizes $f(x) = \frac{1}{2} x^{T}x$.

    1.  Let $\mathbb{Z}$ be a _null-space_ matrix for the matrix $\mathbb{A}$; that
    is, the columns of $\mathbb{Z}$ are a basis of the nullspace of
    $\mathbb{A}$. Prove that if $\nabla f(x^{\star}) = \mathbb{A}^{T}\lambda$
    for some vector $\lambda$, then $\mathbb{Z}^{T}\nabla f(x^{\star}) = 0$.

---

## Homework 4

_Status:_ Final.  

**Due date:** ~~Wednesday~~ Friday, February 21 (during lecture).

From Chong & $\dot{\mathrm{Z}}$ak, _An Introduction to Optimization_.

-   _Regular_ exercises:
    *   _Chapter 10:_ 1, 2, 3, 5, 7, 9, 10.
    *   _Chapter 11:_ 1 a b c & d parts 1 -- 3.

-   _Miscellaneous_ exercises:
    *   _Chapter 10:_ 4, 6, 11 (please reach out to me should you want to make
        your code _available_ to the whole class).
    *   _Chapter 11:_ 1 e, part 4.

        > _Note:_ you may replace MATLAB in 10.11 above with your favorite
        > programming language.

The following problems will not be graded, but you are encouraged to attempt
them as similar problems might appear in midterm 2.

1.  Let $n \ge 2$. Show that the intersection of $n$ convex sets is convex.

    > _Hint:_ Prove first the case when $n=2$, then use induction.

1.  Let $r^{T}$ be a _row_ vector in $\mathbf{R}^{n}$, and let $c$ be an
    arbitrary scalar value. Show that $S=\{x\in\mathbf{R}^{n}\,:\,r^{T}x\le c\}$
    is a convex set.

1.  Let $\mathbb{A}$ be an $m\times n$ matrix with real entries, and let $c$ be
    a _column_ vector in $\mathbf{R}^{m}$. Show that $S = \{x\in\mathbf{R}^{n}\,
    :\,\mathbb{A}x \le c\}$ is convex.
    
    > _Hint:_ Let $r_{1}^{T},\dots,r_{m}^{T}$ denote the _rows_ of $\mathbb{A}$.
    > Argue you why
    > $$S = \bigcap_{j=1}^{m} \{x\in\mathbf{R}^{n}\,:\,r_{j}^{T}x\le c_{j}\},$$
    > where $c_{j}$ represents the $j$-th entry of $c$; then use the previous
    > exercises to conclude $S$ is convex.

1.  Let $\mathbb{A}$ be an $m\times n$ matrix with real entries, and let $c$ be
    a _column_ vector in $\mathbf{R}^{m}$. Show that $S = \{x\in\mathbf{R}^{n}\,
    :\,\mathbb{A}x = c\}$ is convex.

1.  A **convex combination** of $x_{1},\dots,x_{p}$ in $\mathbf{R}^{n}$ is a
    linear combination where the scalars are nonnegative and their sum equals
    one.
    
    In symbols, $y$ is a convex combination of the points $x_{1},\dots,x_{p}$ if
    $$y = \sum_{j=1}^{p} \alpha_{j} x_{j},$$
    where
    $$ \sum_{j=1}^{p} \alpha_{j} = 1,\quad\text{and}\quad \alpha_{j} \ge 0.$$

    Prove that a convex combination of convex functions all defined on the same
    convex set $S$ is also a convex function on $S$.
    
1.  Let $f$ be a convex function on a convex set $S \subset \mathbf{R}^{n}$. Let
    $k$ be a nonzero scalar, and define $g(x) = kf(x)$. Prove that if $k>0$ then
    $g$ is a convex function on $S$.

---

## Homework 3

_Status:_ ~~In progress~~ Final.  

**Due date:** Tuesday, February 11 (during DS).

From Chong & $\dot{\mathrm{Z}}$ak, _An Introduction to Optimization_.

-   _Regular_ exercises:
    *   _Chapter 8:_ 8, 15, 16 & 18.

        > _Note:_ Problem 8 was also in homework 2, however this time you should
        > be a bit more comfortable with it.

    *   _Chapter 9:_ 1 a b & c, 3, 4 a & b.

    In addition please solve the following exercises.

    *   Prove that the function $h_{1}(x) = x - 1 - \log x$ has a unique global
        minimizer on the interval $(0,\infty)$. Then conclude that on this
        interval $\log x \le x -1$.
    *   Prove that the function $h_{2}(x) = \log x -2(x - 1)$ has a unique
        global maximizer on $(0,\infty)$; then use this result to show that on
        the interval $(1/2,1]$, the inequality $2(x-1)\le\log x$ holds.
    *   Prove that under the assumptions of theorem 8.1 for the function $f$, if
        the _fixed-step-size_ algorithm produces a convergent sequence for a
        given $x_{(0)}\neq x^{\star}$, then the step size $\alpha$ satisfies
        $$0<\alpha<\frac{2}{\lambda_{\mathrm{max}}(\mathbf{Q})}.$$

        > _Hint:_ You do not have to prove this from scratch. I am simply asking
        > you to read the proof of Theorem 8.3, and to write your _personalized_
        > proof (_i.e.,_ one that contains your very own argumentations and/or
        > explanations of the _facts_ stated in your textbook).

-   _Miscellaneous_ exercises:
    *   _Chapter 8:_ 8.11 (the proof of theorem 8.1 might come in handy).
    *   _Chapter 9:_ 1 d & e.

---

## Homework 2

_Status:_ ~~In progress~~ Final.  

**Due date:** TBA.

> **Note:** I never actually set a deadline for this assignment here. However I
> did mention during the midterm that you were expected to hand it over to your
> TA the next day. If for some reason you have not handed it over, make sure to
> do so no later than this coming Friday.  

From Chong & $\dot{\mathrm{Z}}$ak, _An Introduction to Optimization_.

-   _Regular_ exercises:
    *   _Chapter 7:_ 7.2 a b & d, 7.4, 7.7, 7.8 a. 
    *   _Chapter 8:_ 8.1, 8.7 a b c & e, 8.

    *   Also, please repeat exercise 7 a b & d, for the function
        $f(x)=8\mathrm{e}^{1-x} + 7 \log x$, where $\log$ represents the natural
        logarithm function.

        > Note that this is in essence exercise 7.3 (where the textbook
        > specifically instructs you to use MATLAB). Feel free to use your
        > favorite software, or even solve this exercise with _pen, paper, and a
        > calculator_.

-   _Miscellaneous_ exercises:
    *   _Chapter 7:_ 7.10 feel free to use your favorite software: MATLAB,
        OCTAVE, python, C++, Ms Excel, etc.
    *   _Chapter 8:_ 8.3, 8.4, 10 a & b.

---

## Homework 1

_Status:_ Final.  

**Due date:** Wednesday, January 15.

From Chong & $\dot{\mathrm{Z}}$ak, _An Introduction to Optimization_.

-   _Regular_ exercises:
    *   _Chapter 6:_ 6.1 a & b, 6.3, 6.4, 6.8, 6.10, 6.11, 6.12, 6.15, 6.16,
        6.25, 6.33.

-   _Miscellaneous_ exercises:
    *   _Chapter 6:_ 6.5, 6.6, 6.14, 6.24, 6.27, 6.32.

---

## Homework 0

_Status:_ ~~In progress~~ Final.  
**Due date:** `N/A` These problems will not be collected nor graded.

From Chong & $\dot{\mathrm{Z}}$ak, _An Introduction to Optimization_.

-   _Miscellaneous_ exercises:
    *   _Chapter 1:_ 1.1, 1.3.
    *   _Chapter 2:_ 2.7, 2.8, 2.9.
    *   _Chapter 3:_ 3.6, 3.18 a b.
    *   _Chapter 4:_ 4.2, 4.3, 4.4. 
    *   _Chapter 5:_ 5.4, 5.5, 5.6, 5.10 a.

---

<!-- This is a comment 

From Saff & Snider, _Fundamentals of Complex Analysis_.

-   _Regular_ exercises:
    *   _Section 1.4:_ 2, 4, 8, 12 b), 16, 20, 22.

    > **Note:** For these exercises, use definition number 5, on page 27.
    > Namely
    > $$\mathrm{e}^{x+iy} := \mathrm{e}^{x}(\cos y + i \sin y).$$

    *   _Section 1.5:_ 5 a) e), 7 a) c), 9 (_Hint:_ What happens when $z=1$?),
        11 (_Hints:_ Divide both sides of the equation by $z^{5}$. Also, notice
        that the equation is of degree 4. Why?).
    *   _Section 1.7:_ 1, 5.
    *   _Section 2.1:_ 1 a) d) e), 4, 8, 9.
    *   _Section 2.2:_ 3, 4, 5. 7 a) b) d).


-   _Miscellaneous_ exercises:
    *   _Section 1.4:_ 11, 19, 21, 23.
    *   Use the equations
        $$\cos\theta = \mathbf{Re}( \mathrm{e}^{i\theta} ) =
        \frac{\mathrm{e}^{i\theta}+\mathrm{e}^{-i\theta}}{2},\qquad
        \sin\theta = \mathbf{Im}( \mathrm{e}^{i\theta} ) =
        \frac{\mathrm{e}^{i\theta}-\mathrm{e}^{-i\theta}}{2i},$$
        to prove the following trigonometric identities
        a)  $\sin^{2}\theta + \cos^{2}\theta = 1$
        a)  $\cos(\theta_{1}+\theta_{2}) = \cos\theta_{1}\cos\theta_{2} -
            \sin\theta_{1}\sin\theta_{2}$
    *   _Section 2.1:_ 12.
    *   _Section 2.2:_ 15, 16.

End of comment. -->


[Return to main course website][MAIN]


[MAIN]: ..


