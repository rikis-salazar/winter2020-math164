# Game plans (by week)

This section used to be called _weekly summaries_. The idea was for me to review
the material covered discussed over the previous week, then post a brief
summary of he ideas/topics discussed during lecture. However, often times
(especially close exam dates) I would find myself behind schedule.

To prevent myself from falling behind, this time I will post here _game plans
for the week_. That is, short descriptions of the topics ideas I intend to
discuss within the next two/three lectures. In a sense, the information listed
below can be regarded as a sort of _study guide_ that should complement the
["[p]ractice material"][pm] section of our class website.

---

## Week 3

On Wednesday we will try to understand a bit better how _steepest descent_
applies to quadratic functions: we'll show that in this particular case, a clsed
formula for the step length $\alpha_{k}$ can be found (_i.e.,_ there is no need
to apply a 1-D search to find the optimal value of $\alpha$). Next, we'll point
out that, given the right conditions, _gradient search algoriths_ (_e.g.,_
steepest descent, fixed step length) do in fact converge to the global
minimizer.

> Please note that we will prove these claims after the midterm.

On Friday, we'll introduce the notion of _order of convergence_. After going
over some examples I might decide to [re]-introduce _Newton's method_ in
$\mathbb{R}^{n}$.

---

## Week 2

> Please note that I will hold 4 lectures during this week to make up for the
> missing lecture on Friday of week 1.

More details to come later. In the meantime please keep in mind that on Tuesday
(01/14) **we will meet at MS 5117** as opposed to the usual lecture room.

*01/13 UPDATE:*

*   We'll briefly go over the course syllabus. Please make sure to read it (in
    case you haven't already) and feel free to ask questions during Monday's
    lecture.

*   Depending on whether or not I feel that reviewing Calculus/Linear algebra is
    needed at this point[^two], I could either proceed with reviewing some
    topics, or start discussing right away contents from chapter 7 (1
    dimensional search methods).

I'll try to have all chapter 7 search methods covered by Friday. Also, keep in
mind that homework 1 is due on Wednesday. 

*01/17 UPDATE:*

*   We'll finish chapter 7 Today and will start discussing the basics of
    _gradient_ search methods in several dimensions.

---

## Week 1

> Please note that I will not be able to teach at all during this week.

*   On Monday (01/06) you will meet with professor _Palina Salanevich_ at the
    usual lecture room (MS 5118).

    The plan is for her to remind you what _optimization_ stands for (_e.g.,_
    Calculus examples, definitions, as well as criteria for the existence of
    extreme points/values). She might choose to present some definitions and/or
    examples from Chapter 6 in your textbook.

    > **01/06 Update:** Prof. Salanevich has been kind enough to provide a brief
    > summary of her first lecture. She mentioned that she
    >
    > *   _"gave a few examples of optimization problems and reminded
    >     definitions of local/global maximizer/minimizer"_;
    > *   _"[formulated] the first and second derivative tests in 1d [and
    >     provided some] intuition behind [them]"_;
    > *   _"formulated a constraint problem in $n$ dimensions"_;
    > *   _"defined [the concepts of] feasible direction, gradient and
    >     Hessian"_; and
    > *   _"explained the intuition behind the FONC, but did not have time to
    >     talk about [Second Order Conditions]"_.

*   On Tuesday (01/07), _Younghak Kwon_ will lead his regularly scheduled
    Discussion Session at MS 5117 (please note that **this is not the usual
    lecture room**).

    You are encouraged to ask questions about [_assignment 0_][hw]. Even though
    this assignment will not be collected, selected questions that emphasize
    specific results might come in handy, and/or appear as exam topics later in
    the quarter.

*   On Wednesday (01/08) you will meet again with professor _Palina Salanevich_
    at the usual lecture room.

    The plan ~~is~~ was for her to start discussing _one dimensional search
    methods_. ~~Ideally she will talk about the ideas behind the _golden section
    search_, as well as the _Fibonacci method_ (this latter algorithm will not
    be studied in detail, only the basic ideas will be presented). If time
    permits, additional methods might be introduced (_e.g.,_ Newton's method,
    secant method, etc.).~~ However, based on the topics she covered during
    lecture one, it seems more appropriate for her to continue discussing
    ideas/proofs from Chapter 6.

*   On Friday (01/10), _Younghak Kwon_ will lead another discussion session;
    however, this time the meeting place will be MS 5118 (_i.e.,_ the usual
    lecture room).

    You are encouraged to ask questions about [_assignment 1_][hw] which will be
    based on ~~one dimensional search methdods~~ when/how to apply the First,
    and Second Order Conditions in one or more dimensions. Unlike homework
    assignment 0, this set of exercises **will be collected and graded**. So,
    make sure to attend this second DS of the week, and ask as many questions as
    you deem appropriate.

---

[Return to main course website][MAIN]


[^two]: Professor Salanevich confided me that it appeared some students were not
  quite familiar with terms like FONC, SONC, SOSC, etc.

[hw]: ../assignments/
[pm]: ../practice/
[MAIN]: ..

